module fshiori.org/alice

go 1.12

require (
	fshiori.org/gosdk v0.0.0-00010101000000-000000000000
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/appleboy/gin-jwt v0.0.0-20190409072159-633d983b91f0
	github.com/appleboy/gofight/v2 v2.1.1
	github.com/bradfitz/gomemcache v0.0.0-20190329173943-551aad21a668
	github.com/casbin/casbin v1.8.2 // indirect
	github.com/couchbase/go-couchbase v0.0.0-20190515160915-dcefcb68da4a // indirect
	github.com/couchbase/gomemcached v0.0.0-20190515232915-c4b4ca0eb21d // indirect
	github.com/couchbase/goutils v0.0.0-20190315194238-f9d42b11473b // indirect
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/elastic/go-elasticsearch/v6 v6.7.0
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-contrib/zap v0.0.0-20190405225521-7c4b822813e7
	github.com/gin-gonic/gin v1.4.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/gofuzz v1.0.0 // indirect
	github.com/jinzhu/gorm v1.9.8
	github.com/kisielk/errcheck v1.2.0 // indirect
	github.com/kr/pty v1.1.4 // indirect
	github.com/labstack/gommon v0.2.9 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/siddontang/ledisdb v0.0.0-20190202134119-8ceb77e66a92 // indirect
	github.com/spf13/viper v1.3.2
	github.com/streadway/amqp v0.0.0-20190404075320-75d898a42a94
	github.com/stretchr/testify v1.3.0
	github.com/swaggo/swag v1.5.0
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/tidwall/pretty v1.0.0 // indirect
	github.com/wendal/errors v0.0.0-20181209125328-7f31f4b264ec // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/build v0.0.0-20190314133821-5284462c4bec // indirect
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
	golang.org/x/net v0.0.0-20190607181551-461777fb6f67 // indirect
	golang.org/x/sys v0.0.0-20190610200419-93c9922d18ae // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190610231749-f8d1dee965f7 // indirect
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0 // indirect
)

replace github.com/ugorji/go v1.1.4 => github.com/ugorji/go/codec v0.0.0-20190204201341-e444a5086c43

replace fshiori.org/gosdk => ./vendor/gosdk
